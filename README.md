README for BTRANS repo

This repository keeps track of code written to analyze fully-coupled global climate model runs: CESM 1.0 with compset BTRANS. It is similar to the ncl_analysis repository which is NCL code written to analyze an uncoupled atmosphere-land and data ocean model run (CESM 1.0 compset FAMIP). Both sets of data are at T85 resolution, but new variables added and signals arising from the changed configuration necessitated different scripts for the different sets of data.

From analysis done for the dissertation of Abigail Gaddis, Univeristy of Tennessee, 2013.
