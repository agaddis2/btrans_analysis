;========================================================================================
; Filename: spatial_signal.ncl
; Description: Analyzes predictability signals of variables from two ensembles of model 
; runs, calculating a signal to noise ratio for the predictability. This is done for all
; grid points in the model, and an alternate signal using EOFs is calculated.
; Date: 8/13/2012
; Created by Abigail Gaddis, University of Tennessee
;========================================================================================

   load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
   load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
   load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"

;----------------------------------------------------------------------------------------
; User settings
;----------------------------------------------------------------------------------------

   ;File labeling and names
   dataset1     = "CESM1.0"
   dataset2     = "CESM1.0 no Pinatubo"

   neval = 3   ;number of eofs to evaluate
   num1var = 4  ; first non-dimension variable in the file to analyze

;----------------------------------------------------------------------------------------
; Read in list of files
;----------------------------------------------------------------------------------------

   ;read in BTRANS ensemble anomaly files
   dir = "./BTRANS/"
   fil = systemfunc("cd "+dir+"  ; ls decAnom*clm*")
;   fil = systemfunc("cd "+dir+"  ; ls *clm*")
   filelist = addfiles(dir+fil, "r")
   vNames = getfilevarnames (filelist[0]) ; get names of all variables in file
   nNames = dimsizes (vNames)   ; number of variables on the file

   ;read in BTRANS minus Pinatubo ensemble anomaly files
   noPindir = "./noPin/"
   noPinfil = systemfunc("cd "+noPindir+"  ; ls decAnom*clm*")
;   noPinfil = systemfunc("cd "+noPindir+"  ; ls *clm*")
   noPinfilelist = addfiles(noPindir+noPinfil, "r")

;----------------------------------------------------------------------------------------
; Read in variables over selected time periods
;----------------------------------------------------------------------------------------

   lat = filelist[0]->lat
   lon = filelist[0]->lon
   time = filelist[0]->time
   ens = fil
   nlats = dimsizes(lat)
   nlons = dimsizes(lon)
   nmon = dimsizes(time)
   nens = dimsizes(fil)
   wgt = NormCosWgtGlobe(lat)
   npts = nlats*nlons   ;number of data points per level in final plotted variable - for missing value calc

; Initializing variables

   vars_BTRANS3D = new ((/nens, nmon, nlats, nlons/), float)
   vars_BTRANS3D!0 = "ens"
   vars_BTRANS3D!1 = "time"
   vars_BTRANS3D!2 = "lat"
   vars_BTRANS3D!3 = "lon"
   vars_BTRANS3D&ens = ens
   vars_BTRANS3D&time = time
   vars_BTRANS3D&lat = lat
   vars_BTRANS3D&lon = lon
   vars_BTRANS3D = 0.0
   vars_noPin3D = vars_BTRANS3D
   season_B = new ((/nens, 2, nlats, nlons/), float)
   season_n = season_B
   evec_BTRANS3D = new ((/neval, nlats, nlons/), double)
   evec_noPin3D = evec_BTRANS3D
   evects_BTRANS3D = new ((/neval,nmon/), double)
   evects_noPin3D = evects_BTRANS3D
   ensembleav_BTRANS3D = vars_BTRANS3D(0,:,:,:)
   ensembleav_noPin3D = vars_noPin3D(0,:,:,:)
   t_val = vars_BTRANS3D(0,:,:,:)
   eofdiff3D = new((/neval,nlats,nlons/), double)
   eofdiff3D!1 = "lat"
   eofdiff3D!2 = "lon"
   eofdiff3D&lat = lat
   eofdiff3D&lon = lon

; Read all variables in the ensemble files

   do n= num1var,nNames-1   ;read all variables in the file, variable 4 is the first 3D variable
      varname = vNames(n)
      print ("Calculating spatial predictability t values for variable " + varname)
      print ("Variable is " + n + " out of " + (nNames-1))

      outputfile = varname+"_spat_ttest_lnd"
      vardims = getfilevardims(filelist[0],varname) ;gets dim names of nth variable in the first filelist file
      vardimnum = dimsizes(vardims)                     ;finds number of dims in nth variable

      ; Read in anomaly for each file for this variable
      do i = 0,nens-1
         vars_BTRANS3D(i,:,:,:) = filelist[i]->$varname$
         vars_noPin3D(i,:,:,:) = noPinfilelist[i]->$varname$
      end do

   ;----------------------------------------------------------------------------------------
   ; Calculate ensemble averages, EOFs, and the time series of the eigenvalues
   ;----------------------------------------------------------------------------------------

      ensembleav_BTRANS3D = dim_avg_n_Wrap(vars_BTRANS3D,0)
      ensembleav_noPin3D = dim_avg_n_Wrap(vars_noPin3D,0)

      timeAv_BTRANS3D = dim_avg_n_Wrap(vars_BTRANS3D(:,3:39,:,:),(/0,1/))
      sprAv_BTRANS3D = dim_avg_n_Wrap(month_to_season(ensembleav_BTRANS3D(10:33,:,:),"MAM"),0)
      sumAv_BTRANS3D = dim_avg_n_Wrap(month_to_season(ensembleav_BTRANS3D(10:33,:,:),"JJA"),0)
      fallAv_BTRANS3D = dim_avg_n_Wrap(month_to_season(ensembleav_BTRANS3D(10:33,:,:),"SON"),0)
      winAv_BTRANS3D = dim_avg_n_Wrap(month_to_season(ensembleav_BTRANS3D(10:33,:,:),"DJF"),0)

      timeAv_noPin3D = dim_avg_n_Wrap(vars_noPin3D(:,3:39,:,:),(/0,1/))
      sprAv_noPin3D = dim_avg_n_Wrap(month_to_season(ensembleav_noPin3D(10:33,:,:),"MAM"),0)
      sumAv_noPin3D = dim_avg_n_Wrap(month_to_season(ensembleav_noPin3D(10:33,:,:),"JJA"),0)
      fallAv_noPin3D = dim_avg_n_Wrap(month_to_season(ensembleav_noPin3D(10:33,:,:),"SON"),0)
      winAv_noPin3D = dim_avg_n_Wrap(month_to_season(ensembleav_noPin3D(10:33,:,:),"DJF"),0)


      wgtEnsAv_BTRANS3D = ensembleav_BTRANS3D*conform(ensembleav_BTRANS3D, wgt, 1)
      wgtEnsAv_noPin3D = ensembleav_noPin3D*conform(ensembleav_noPin3D, wgt, 1)
      copy_VarMeta(vars_BTRANS3D(0,:,:,:),wgtEnsAv_BTRANS3D)
      copy_VarMeta(vars_noPin3D(0,:,:,:),wgtEnsAv_noPin3D)

      evec_BTRANS3D = eofunc_Wrap(wgtEnsAv_BTRANS3D(lat|:,lon|:,time|:),neval,False)
      evec_noPin3D = eofunc_Wrap(wgtEnsAv_noPin3D(lat|:,lon|:,time|:),neval,False)
      evects_BTRANS3D = eofunc_ts_Wrap(wgtEnsAv_BTRANS3D(lat|:,lon|:,time|:),evec_BTRANS3D,False)
      evects_noPin3D = eofunc_ts_Wrap(wgtEnsAv_noPin3D(lat|:,lon|:,time|:),evec_noPin3D,False)

      pcvar = evec_BTRANS3D@pcvar

   ;----------------------------------------------------------------------------------------
   ; Calculate variance and t-values on lat/lon grid for different time averages
   ;----------------------------------------------------------------------------------------
      ; t-value(time,lat,lon) for plotting specific times

      var_noPin3D = dim_variance_n_Wrap(vars_noPin3D,0)
      var_BTRANS3D = dim_variance_n_Wrap(vars_BTRANS3D,0)
      s = nens
      ;Welch's t-test applied since variances are not equal
      iflag = True
      t_prob3D = ttest(ensembleav_BTRANS3D,var_BTRANS3D,s,ensembleav_noPin3D,var_noPin3D,s,iflag,True)
      ; t-value(lat,lon) time average of all months for three years after eruption
      s2 = nens * (39-3)
      var_tAv_B = dim_variance_n_Wrap(vars_BTRANS3D(:,3:39,:,:),(/0,1/))
      var_tAv_n = dim_variance_n_Wrap(vars_noPin3D(:,3:39,:,:),(/0,1/))
      t_tAv_prob = ttest(timeAv_BTRANS3D,var_tAv_B,s2,timeAv_noPin3D,var_tAv_n,s2,iflag,True)

      ; t-value(lat,lon) average spring for two years after eruption
      
      s3 = nens*2
      do i = 0,nens-1 
         season_B(i,:,:,:) = month_to_season(vars_BTRANS3D(i,10:33,:,:),"MAM")
         season_n(i,:,:,:) = month_to_season(vars_noPin3D(i,10:33,:,:),"MAM")
      end do
      var_sprAv_B = dim_variance_n_Wrap(season_B,(/0,1/))
      var_sprAv_n = dim_variance_n_Wrap(season_n,(/0,1/))
      t_sprAv_prob = ttest(sprAv_BTRANS3D,var_sprAv_B,s3,sprAv_noPin3D,var_sprAv_n,s3,iflag,True)

      ; t-value(lat,lon) average summer for two years after eruption

      do i = 0,nens-1
         season_B(i,:,:,:) = month_to_season(vars_BTRANS3D(i,10:33,:,:),"JJA")
         season_n(i,:,:,:) = month_to_season(vars_noPin3D(i,10:33,:,:),"JJA")
      end do
      var_sumAv_B = dim_variance_n_Wrap(season_B,(/0,1/))
      var_sumAv_n = dim_variance_n_Wrap(season_n,(/0,1/))
      t_sumAv_prob = ttest(sumAv_BTRANS3D,var_sumAv_B,s3,sumAv_noPin3D,var_sumAv_n,s3,iflag,True)

      ; t-value(lat,lon) average fall for two years after eruption
      do i = 0,nens-1
         season_B(i,:,:,:) = month_to_season(vars_BTRANS3D(i,10:33,:,:),"SON")
         season_n(i,:,:,:) = month_to_season(vars_noPin3D(i,10:33,:,:),"SON")
      end do
      var_fallAv_B = dim_variance_n_Wrap(season_B,(/0,1/))
      var_fallAv_n = dim_variance_n_Wrap(season_n,(/0,1/))
      t_fallAv_prob = ttest(fallAv_BTRANS3D,var_fallAv_B,s3,fallAv_noPin3D,var_fallAv_n,s3,iflag,True)

      ; t-value(lat,lon) average winter for two years after eruption
      do i = 0,nens-1
         season_B(i,:,:,:) = month_to_season(vars_BTRANS3D(i,10:33,:,:),"DJF")
         season_n(i,:,:,:) = month_to_season(vars_noPin3D(i,10:33,:,:),"DJF")
      end do
      var_winAv_B = dim_variance_n_Wrap(season_B,(/0,1/))
      var_winAv_n = dim_variance_n_Wrap(season_n,(/0,1/))
      t_winAv_prob = ttest(winAv_BTRANS3D,var_winAv_B,s3,winAv_noPin3D,var_winAv_n,s3,iflag,True)

   ;----------------------------------------------------------------------------------------
   ; Use output to create plots
   ;----------------------------------------------------------------------------------------

   ; General plotting settings
      wks          = gsn_open_wks("pdf",outputfile)
      plot = new(6, graphic)
      plot1 = new(3,graphic)
      plot2 = plot1
      plot_Avs = new(5, graphic)
      plot3 = new(neval,graphic)
      plot4 = plot3

      gsn_define_colormap(wks,"BlueDarkRed18")

      months = (/"July 1991","Feb 1992","Sept 1992","Apr 1993","Nov 1993","Jun 1994"/)

   ; Contour plot resources
      rescn = True
      rescn@cnFillOn                 = True
      rescn@gsnSpreadColors          = True
      rescn@gsnAddCyclic             = True
      rescn@cnLinesOn                = False        ; True is default
      rescn@cnLineLabelsOn           = False        ; True is default
      rescn@cnInfoLabelOn            = False
      rescn@tmLabelAutoStride        = True
      rescn@tmYROn                   = False
      rescn@tmXTOn                   = False
      rescn@tiMainString             = ""

    ; Time series plot resources
      rts                            = True
      rts@tiXAxisString              = "Time (years)"
      rts@tiYAxisString              = "Forced ensemble EOF amplitude"
      rts@tmLabelAutoStride          = True
      rts@tmXTOn                     = False
      rts@tmYROn                     = False
      rts@gsnYRefLine                = 0.0
      rts@tmXBMode                   = "Explicit"
      rts@tmXBValues               = (/time(0),time(11),time(23),time(35),time(47),time(52)/)
      rts@tmXBLabels               = (/"Mar 91","Mar 92","Mar 93","Mar 94","Mar 95","Jul 95"/)
      rts@tmXBMinorValues            = time

    ; Panel resources
      resP                       = True                   ; modify the panel plot
      resP@gsnMaximize           = True                   ; use full page
      resP1                      = resP                   ; panel resources for contour plots
      resP1@gsnPanelLabelBar     = True                   ; add common colorbar to contour panel
      resP1@lbAutoManage         = False
      resP1@tmLabelAutoStride    = True
      resP1@lbLabelAutoStride    = True
      resP1@lbLabelFontHeightF   = 0.010
      resP2                      = resP1

     ; Individual contour plot resources
      rescn1                      = rescn
      rescn1@gsnDraw              = False       ; don't draw yet
      rescn1@gsnFrame             = False       ; don't advance frame yet
      rescn1@gsnLeftString        = ""
      rescn1@lbLabelBarOn         = False
;      rescn1@cnLevelSelectionMode = "ManualLevels"
;      rescn1@cnMinLevelValF       = 0
;      rescn1@cnMaxLevelValF       = max(signal3D)
;      rescn1@cnLevelSpacingF      = max(signal3D)/20
      resP1@txString              = vars_BTRANS3D@long_name+ " t-value"

      t_val = (/t_prob3D(1,:,:,:)/)

;      if (all(ismissing(signal3D)) .or. (min(signal3D) .eq. max(signal3D)))  then
;         print("Variable contains only missing values or signal max is less than 6. Skipping.")
;        ; deleting plotting resources
;         delete (plot)
;         delete (plot3)
;         delete (wks)
        ; deleting variables that change size if 3D vs 4D
;         delete(vardims)
;         continue
;      else
         count = 0
         do t = 6,47,7
            rescn1@gsnRightString = months(count)
            plot(count) = gsn_csm_contour_map(wks,t_val(time|t,lat|:,lon|:),rescn1)
            count = count+1
         end do
         do p=0,2
            plot1(p) = plot(p)
            plot2(p) = plot(p+3)
         end do

;         rescn@gsnDraw              = False       ; don't draw yet
;         rescn@gsnFrame             = False       ; don't advance frame yet
         rescn@gsnRightString = "Two year time average"
         plot_Avs(0) = gsn_csm_contour_map(wks,t_tAv_prob(1,:,:),rescn)
         rescn@gsnRightString = "Two year spring average"
         plot_Avs(1) = gsn_csm_contour_map(wks,t_sprAv_prob(1,:,:),rescn)
         rescn@gsnRightString = "Two year summer average"
         plot_Avs(2) = gsn_csm_contour_map(wks,t_sumAv_prob(1,:,:),rescn)
         rescn@gsnRightString = "Two year fall average"
         plot_Avs(3) = gsn_csm_contour_map(wks,t_fallAv_prob(1,:,:),rescn)
         rescn@gsnRightString = "Two year winter average"
         plot_Avs(4) = gsn_csm_contour_map(wks,t_winAv_prob(1,:,:),rescn)


         ; Retrieve contour levels.
;         getvalues plot1@contour               "cnLevels" : levels
;         end getvalues
;         resP1@lbLabelStrings       = sprintf("%4.0f",levels)   ; Format the labels

         gsn_panel(wks,plot1,(/3,1/),resP1)
         gsn_panel(wks,plot2,(/3,1/),resP1)
;      end if

   ; EOF coefficient(k) time series and EOF(k) difference plots
      rts@gsnDraw                 = False       ; don't draw yet
      rts@gsnFrame                = False       ; don't advance frame yet
      rts@gsnScale                = True        ; force text scaling
      rescn2                      = rescn
      rescn2@gsnDraw              = False       ; don't draw yet
      rescn2@gsnFrame             = False       ; don't advance frame yet
      rescn2@gsnRightString       = ""
      rescn2@lbLabelBarOn         = True
      rescn2@cnLevelSelectionMode =  "AutomaticLevels"
      rescn2@tmLabelAutoStride    = True
      rescn2@lbLabelAutoStride    = True
      resP@txString               = vars_BTRANS3D@long_name

      if (all(ismissing(eofdiff3D)) .or. (min(eofdiff3D) .eq. max(eofdiff3D)))  then
         print("Variable EOF contains only missing values. Skipping.")
         delete (wks)
         delete(vardims)
         continue
      else if (all(ismissing(evects_BTRANS3D)) .or. (min(evects_BTRANS3D) .eq. max(evects_BTRANS3D)))  then
         print("Variable EOF contains only missing values. Skipping.")
         delete (wks)
         delete(vardims)
         continue
      else
         do k=0,neval-1
            rescn2@gsnLeftString = "EOF "+(k+1)
            rts@gsnLeftString = "EOF "+(k+1)
            rts@gsnRightString = "% variance = " + sprintf("%3.1f", pcvar(k))
            plot3(k) = gsn_csm_xy (wks,time,evects_BTRANS3D(k,:),rts)
            plot4(k) = gsn_csm_contour_map(wks,evec_BTRANS3D(k,:,:),rescn2)        ;eofdiff3D(k,:,:),rescn2)
         end do
         gsn_panel(wks,plot3,(/neval,1/),resP)
         gsn_panel(wks,plot4,(/neval,1/),resP)
      end if
      end if

   ; deleting variables that change size
      delete (plot)
      delete (plot1)
      delete (plot2)
      delete (plot3)
;      delete (levels)
      delete (wks)
      delete(vardims)
   end do

